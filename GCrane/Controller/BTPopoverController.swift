//
//  BTPopoverController.swift
//  GCrane
//
//  Created by Holger Kremmin on 21.04.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import UIKit
import XCGLogger

class BTPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    let log = XCGLogger.default
    var btCrane = BTCrane.sharedInstance

    //UI
    @IBOutlet weak var devicePicker: UIPickerView!
    
    
    override func viewDidLoad() {
        devicePicker.delegate = self
        devicePicker.dataSource = self
        //check if already scanned for BT devices
        if !btCrane.scanPerformed {
            btCrane.scan4Devices()
            btCrane.scanFinishedHandler = { [unowned self] in
                self.devicePicker.reloadAllComponents()
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        devicePicker.reloadAllComponents()
    }
    
    //Button actions
    @IBAction func connectButtonPressed(_ sender: Any) {
        
        btCrane.connect(selected: devicePicker.selectedRow(inComponent: 0))
    }
    
    @IBAction func scanButtonPressed(_ sender: Any) {
        btCrane.scan4Devices()
        btCrane.scanFinishedHandler = { [unowned self] in
            self.devicePicker.reloadAllComponents()
        }
    }
    
    //picker delegates
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 //just one column
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //log.debug("bt picker per count = \(btCrane.peripherals.count)")
        return btCrane.peripherals.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let device = btCrane.peripherals[row]
        guard let title = device.name else {
            return "unknown"
        }
        
        return title
    }
    
}
