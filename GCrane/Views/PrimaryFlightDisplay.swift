//
//  PrimaryFlightDisplayScene.swift
//  PrimaryFlightDisplay
//
//  Created by Michael Koukoullis on 21/11/2015.
//  Copyright © 2015 Michael Koukoullis. All rights reserved.
//

import SpriteKit
import XCGLogger

public class PrimaryFlightDisplayView: SKView {
    
    let log = XCGLogger.default
    
    var settings: SettingsType = DefaultSettings() {
        
        didSet {
            /*
            if let scene = scene as? PrimaryFlightDisplayScene {
                scene.updateViewElements(settings: settings)
            }
            */
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        log.debug("init flightscene")
        //self.log.debug("flight scene bound = \(bounds)")
        self.allowsTransparency = true
        settings.headingIndicator.size.width = bounds.width
        settings.headingIndicator.size.height = 30
        settings.headingIndicator.minorMarkerFrequency = 1
        settings.headingIndicator.majorMarkerFrequency = 10
        settings.headingIndicator.pointsPerUnitValue = 8
        settings.headingIndicator.markerTextOffset = 15
        
        settings.attitudeReferenceIndex.sideBarWidth = 40
        settings.attitudeReferenceIndex.sideBarHeight = 15
        
        settings.bankIndicator.arcRadius = 100
        settings.altimeter.size = CGSize(width: 60, height: 220)
        settings.airSpeedIndicator.size = CGSize(width: 60, height: 220)
        settings.pitchLadder.magnitudeDisplayDegree = 40
        
        settings.horizon.groundColor = UIColor.lightGray.withAlphaComponent(0.75)
        settings.horizon.skyColor = UIColor.gray.withAlphaComponent(0.75)
        
        let scene = PrimaryFlightDisplayScene(size: bounds.size, settings: settings)
        scene.scaleMode = .aspectFill
        scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        scene.backgroundColor = .clear
        presentScene(scene)
        
        // Apply additional optimizations to improve rendering performance
        ignoresSiblingOrder = true        
    }
    
    public func setHeadingDegree(degree: Double) {
        if let scene = scene as? PrimaryFlightDisplayScene {
            scene.setHeadingDegree(degree: degree)
        }
    }
    
    public func setAirSpeed(airSpeed: Double) {
        if let scene = scene as? PrimaryFlightDisplayScene {
            scene.setAirSpeed(airSpeed: airSpeed)
        }
    }

    public func setAltitude(altitude: Double) {
        if let scene = scene as? PrimaryFlightDisplayScene {
            scene.setAltitude(altitude: altitude)
        }
    }
    
    public func setAttitude(rollRadians: Double, pitchRadians: Double) {
        if let scene = scene as? PrimaryFlightDisplayScene {
            scene.setAttitude(attitude: Attitude(pitchRadians: pitchRadians, rollRadians: rollRadians))
        }
    }
}

class PrimaryFlightDisplayScene: SKScene {
    
    private var horizon: Horizon?
    private var pitchLadder: PitchLadder?
    private var attitudeReferenceIndex: AttitudeReferenceIndex?
    private var bankIndicator: BankIndicator?
    private var altimeter: TapeIndicator?
    private var airSpeedIndicator: TapeIndicator?
    private var headingIndicator: TapeIndicator?
    private var sceneSize:CGSize?
    
    init(size: CGSize, settings: SettingsType) {
        sceneSize = size
        horizon = Horizon(sceneSize: sceneSize!, style: settings.horizon)
        pitchLadder = PitchLadder(sceneSize: sceneSize!, style: settings.pitchLadder)
        attitudeReferenceIndex = AttitudeReferenceIndex(style: settings.attitudeReferenceIndex)
        bankIndicator = BankIndicator(style: settings.bankIndicator)
        altimeter = TapeIndicator(style: settings.altimeter)
        airSpeedIndicator = TapeIndicator(style: settings.airSpeedIndicator)
        headingIndicator = TapeIndicator(style: settings.headingIndicator)
        super.init(size: size)
       print("sksize = \(size)")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateViewElements(settings: SettingsType) {
        horizon = Horizon(sceneSize: sceneSize!, style: settings.horizon)
        pitchLadder = PitchLadder(sceneSize: sceneSize!, style: settings.pitchLadder)
        attitudeReferenceIndex = AttitudeReferenceIndex(style: settings.attitudeReferenceIndex)
        bankIndicator = BankIndicator(style: settings.bankIndicator)
        altimeter = TapeIndicator(style: settings.altimeter)
        airSpeedIndicator = TapeIndicator(style: settings.airSpeedIndicator)
        headingIndicator = TapeIndicator(style: settings.headingIndicator)
    }

    override func didMove(to view: SKView) {
        scaleMode = .resizeFill
        addChild(horizon!)
        addChild(pitchLadder!)
        addChild(attitudeReferenceIndex!)
        addChild(bankIndicator!)
        //addChild(altimeter!)
        //addChild(airSpeedIndicator!)
        addChild(headingIndicator!)
    }
    
    override func didChangeSize(_ oldSize: CGSize) {
        altimeter!.position = CGPoint(x: size.width/2 - (altimeter?.style.size.width)!/2, y: 0)
        airSpeedIndicator!.position = CGPoint(x: -size.width/2 + airSpeedIndicator!.style.size.width/2, y: 0)
        headingIndicator!.position = CGPoint(x: 0, y: size.height/2 - headingIndicator!.style.size.height/2)
    }
    
    override func didEvaluateActions() {
        altimeter!.recycleCells()
        airSpeedIndicator!.recycleCells()
        headingIndicator!.recycleCells()
    }
    
    func setHeadingDegree(degree: Double) {
        headingIndicator!.value = degree
    }
    
    func setAltitude(altitude: Double) {
        altimeter!.value = altitude
    }

    func setAirSpeed(airSpeed: Double) {
        airSpeedIndicator!.value = airSpeed
    }
}

extension PrimaryFlightDisplayScene: AttitudeSettable {

    func setAttitude(attitude: AttitudeType) {
        horizon!.setAttitude(attitude: attitude)
        pitchLadder!.setAttitude(attitude: attitude)
        bankIndicator!.setAttitude(attitude: attitude)
    }
}
