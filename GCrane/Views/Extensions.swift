//
//  Extensions.swift
//  GCrane
//
//  Created by Holger Kremmin on 20.04.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}
