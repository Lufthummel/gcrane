//
//  HorizonExtensions.swift
//  GCrane
//
//  Created by Holger Kremmin on 25.04.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import SpriteKit

extension CGFloat {
    
    static var degreesPerRadian: CGFloat {
        return CGFloat(180.0 / Double.pi)
    }
    
    static var radiansPerDegree: CGFloat {
        return CGFloat(Double.pi / 180.0)
    }
}

extension CGSize {
    
    var pointsPerDegree: CGFloat {
        return (height / 2) / CGFloat(90)
    }
    
    var pointsPerRadian: CGFloat {
        return CGFloat.degreesPerRadian * pointsPerDegree
    }
}

extension Double {
    var compassValue: Double {
        var value = self.truncatingRemainder(dividingBy: 360)
        
        if value < 0 {
            value = 360 + value
        }
        
        return value
    }
}

extension Int {
    
    var radians: CGFloat {
        return CGFloat.radiansPerDegree * CGFloat(self)
    }
    
    var compassValue: Double {
        return Double(self).compassValue
    }
}

