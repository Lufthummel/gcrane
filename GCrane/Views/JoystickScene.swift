//
//  JoystickScene.swift
//  GCrane
//
//  Created by Holger Kremmin on 04.04.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

//
//  GameScene.swift
//
//  Created by Dmitriy Mitrophanskiy on 28.09.14.
//  Copyright (c) 2014 Dmitriy Mitrophanskiy. All rights reserved.
//
import SpriteKit
import XCGLogger


class JoystickScene: SKScene {
    
    let log = XCGLogger.default
    
    
    let leftJoystick = AnalogJoystick(diameter: 120) // from Emoji
    let rightJoystick = AnalogJoystick(diameter: 120) // from Class
    let jSubstrateImage =  UIImage(named: "jSubstrate")
    let jStickImageRed = UIImage(named: "jstickred")
    let jStickImageGreen = UIImage(named: "jstickgreen")
    
    override init(size: CGSize) {
        super.init(size: size)
        log.debug("init joystickscene")
        leftJoystick.stick.image = jStickImageRed
        rightJoystick.stick.image = jStickImageGreen
        leftJoystick.substrate.image = jSubstrateImage
        rightJoystick.substrate.image = jSubstrateImage
        
        leftJoystick.axes = .allaxis
        leftJoystick.interval = 0.2 //as per crane spec
        rightJoystick.axes = .yaxis
        rightJoystick.interval = 0.2 //as per crane spec
        
        //backgroundColor = .green
        
        //addJoystickHandlers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    public func addJoystickHandlers(leftHandler: @escaping ((AnalogJoystickData) -> ()), rightHandler: @escaping ((AnalogJoystickData) -> ()) ) {
        //MARK: Handlers begin
        
        
        leftJoystick.beginHandler = { [unowned self] in
            
            self.log.debug("move")
        }
        /*
        leftJoystick.trackingHandler = { [unowned self] data in
            
            self.log.debug("x.velo =  \(data.velocity.x)  y.velo = \(data.velocity.y)")
            //self.log.debug("angular = \(Double(data.angular) * (180.0 / Double.pi))  x Anteil \(cos(data.angular))  y Anteil  \(sin(data.angular))")
        }
        */
        
        leftJoystick.trackingHandler = leftHandler
        
        leftJoystick.stopHandler = { [unowned self] in
            
            self.log.debug("stopped")
        }
        
        /*
        rightJoystick.trackingHandler = { [unowned self] jData in
            self.log.debug("angular = \(Double(jData.angular) * (180.0 / Double.pi))  x Anteil \(cos(jData.angular))  y Anteil  \(sin(jData.angular))")
        }
        */
        
        rightJoystick.trackingHandler = rightHandler
        
        rightJoystick.stopHandler =  { [unowned self] in
            self.log.debug("rotate stoped")
            
        }
        
        //MARK: Handlers end
    }
    
    /* Setup your scene here */
    override func didMove(to view: SKView) {
        
        
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        leftJoystick.position = CGPoint(x: leftJoystick.radius + 15, y: leftJoystick.radius + 15)
        addChild(leftJoystick)
        rightJoystick.position = CGPoint(x: self.frame.maxX - rightJoystick.radius - 15, y:rightJoystick.radius + 15)
        addChild(rightJoystick)
        
        
        let selfHeight = frame.height
        let btnsOffset: CGFloat = 10
        let btnsOffsetHalf = btnsOffset / 2

        view.isMultipleTouchEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /* Called when a touch begins */
        
        if let touch = touches.first {
            let node = atPoint(touch.location(in: self))

        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func setJoystickAxes(left:JoystickAxes, right:JoystickAxes) {
        leftJoystick.axes = left
        rightJoystick.axes = right
        
    }
}

