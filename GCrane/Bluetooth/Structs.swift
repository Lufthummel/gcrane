//
//  Structs.swift
//  GCrane
//
//  Created by Holger Kremmin on 02.03.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import Foundation

struct BTMessage {
    var channel:UInt8 = 0
    var cmd:UInt8 = 0
    var data:UInt16 = 0
    var index:UInt8 = 0
    var crc:UInt16 = 0
    var isValid = false
    
    
    init(channel:UInt8, cmd:UInt8, index:UInt8, data:UInt16) {
        self.channel = channel
        self.cmd = cmd
        self.index = index
        self.data = data
        initCRC()
        isValid = true
    }
    
    
    init(channel:UInt8, cmd:UInt8, index:UInt8, data:UInt16, crc:UInt16) {
        self.channel = channel
        self.cmd = cmd
        self.index = index
        self.data = data
        self.crc = crc
        isValid = isValidMessage()
    }
    
    init() {
        //should not allow this in future
    }
    
    
    //convert signed int into float and divide by 100 to get degree of axis
    func data2float() -> Float {
        let tmp : Int
        if data > UInt16.max / 2 {
            tmp = -Int(~data + 1) // ~ is the Bitwise NOT Operator
        } else {
            tmp = Int(data)
        }
        
        return Float(tmp)/100.0
    }
    
    //calculate the CRC (ccitt-xmodem)
    private func crc16ccitt(data: [UInt8],seed: UInt32 = 0x0000, final: UInt32 = 0xffff)->UInt16{
        var crc = seed
        data.forEach { (byte) in
            crc ^= UInt32(byte) << 8
            (0..<8).forEach({ _ in
                crc = (crc & UInt32(0x8000)) != 0 ? (crc << 1) ^ 0x1021 : crc << 1
            })
        }
        return UInt16(crc & final)
    }
    
    //init the crc value
    private mutating func initCRC() {
        let tmp:[UInt8] = [channel, cmd, index, UInt8(data >> 8), UInt8(data & 0x00ff)]
        crc = crc16ccitt(data: tmp)
    }
    
    //check if the given message is a valid one (used for responses only)
    private mutating func isValidMessage() -> Bool {
        let tmp:[UInt8] = [channel, cmd, index, UInt8(data >> 8), UInt8(data & 0x00ff)]
        return crc == crc16ccitt(data: tmp)
    }
    
    //debug purposes
    func toString() -> String {
        return "BLECH = " + String(format:"%02X", self.channel) + " CMD = " + String(format:"%02X", self.cmd) + " Index = " + String(format:"%02X", self.index) + " Data = " + String(format:"%02X", self.data) + " CRC = " + String(format:"%02X", self.crc)
    }
    
    mutating func toData() -> Data {
        let d = NSMutableData()
        
        var dataHigh = data.highByte
        var dataLow = data.lowByte
        var crcHigh = crc.highByte
        var crcLow = crc.lowByte

        //convert struct vars into data and append
        let channelData = Data(bytes: &channel, count: MemoryLayout.size(ofValue: channel))
        let cmdData = Data(bytes: &cmd, count: MemoryLayout.size(ofValue: cmd))
        //to get correct endian
        let dataDataHigh = Data(bytes: &dataHigh, count: MemoryLayout.size(ofValue: dataHigh))
        let dataDataLow = Data(bytes: &dataLow, count: MemoryLayout.size(ofValue: dataLow))
        let indexData = Data(bytes: &index, count: MemoryLayout.size(ofValue: index))
        let crcDataHigh = Data(bytes: &crcHigh, count: MemoryLayout.size(ofValue: crcHigh))
        let crcDataLow = Data(bytes: &crcLow, count: MemoryLayout.size(ofValue: crcLow))
        d.append(channelData)
        d.append(cmdData)
        d.append(indexData)
        d.append(dataDataHigh)
        d.append(dataDataLow)
        d.append(crcDataHigh)
        d.append(crcDataLow)

        return d as Data
    }
    
}

//holds the state of the crane
struct CraneStat {
    var roll:Float = 0.0
    var tilt:Float = 0.0  //pitch
    var pan:Float = 0.0  //yaw
    var battery:Float = 0.0
    var modus:UInt16 = 0
}

struct BTPeripheralInfo {
    var manufactor:String = ""
    var model:String = ""
    var serial:String = ""
    var hwRevision:String = ""
    var firmware:String = ""
    var swRevision:String = ""
    var ieee:Data?
    var pnpID:Data?
    var systemID:Data?
    
    init(manufactor:String? = nil, model:String? = nil, serial:String? = nil, hwRevision:String? = nil, firmware:String? = nil, swRevision:String? = nil,
        ieee:Data? = nil, pnpID:Data? = nil, systemID:Data? = nil) {
      //should do some useful stuff here...
        self.manufactor = manufactor!
        self.model = model!
        self.serial = serial!
        self.hwRevision = hwRevision!
        self.firmware = firmware!
        self.swRevision = swRevision!
        self.ieee = ieee
        self.pnpID = pnpID
        self.systemID = systemID
    }
    
}
