//
//  BTPeripheralInfo.swift
//  GCrane
//
//  Created by Holger Kremmin on 01.05.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import Foundation
import XCGLogger
import SwiftyBluetooth
import CoreBluetooth

extension BTCrane {
    
    // returns Device Information org.bluetooth.service.device_information
    func getPeripheralInfo() -> BTPeripheralInfo {
        
        var btInfo = BTPeripheralInfo()
        
        //Manufactor
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A29", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.manufactor = String(data: data, encoding: String.Encoding.utf8) ?? "unknown"
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.manufactor = "error"
                break // An error happened while attempting to read the data
            }
        }
        
        //Model number
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A24", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.model = String(data: data, encoding: String.Encoding.utf8) ?? "unknown"
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.model = "error"
                break // An error happened while attempting to read the data
            }
        }
        
        //Serial
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A25", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.serial = String(data: data, encoding: String.Encoding.utf8) ?? "unknown"
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.serial = "error"
                break // An error happened while attempting to read the data
            }
        }
        
        //Hardware Revision
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A27", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.hwRevision = String(data: data, encoding: String.Encoding.utf8) ?? "unknown"
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.hwRevision = "error"
                break // An error happened while attempting to read the data
            }
        }
        
        //Firmware
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A26", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.firmware = String(data: data, encoding: String.Encoding.utf8) ?? "unknown"
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.firmware = "error"
                break // An error happened while attempting to read the data
            }
        }
        
        //Software Revison
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A28", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.swRevision = String(data: data, encoding: String.Encoding.utf8) ?? "error"
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                break // An error happened while attempting to read the data
            }
        }
        
        // IEEE
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A2A", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.ieee = data
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.ieee = nil
                break // An error happened while attempting to read the data
            }
        }
        
        //PnPID
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A50", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.pnpID = data
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.pnpID = nil
                break // An error happened while attempting to read the data
            }
        }
        
        //System ID
        connectedPeripheral?.readValue(ofCharacWithUUID: "2A23", fromServiceWithUUID: "180A") { result in
            switch result {
            case .success(let data):
                btInfo.systemID = data
            break // The data was read and is returned as an NSData instance
            case .failure(let error):
                self.log.debug("error = \(error)")
                btInfo.systemID = nil
                break // An error happened while attempting to read the data
            }
        }
        
        return btInfo
    }

}
