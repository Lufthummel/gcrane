//
//  BTCrane.swift
//  GCrane
//
//  Created by Holger Kremmin on 01.04.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import Foundation

import XCGLogger
import SwiftyBluetooth
import CoreBluetooth

class BTCrane {
    let log = XCGLogger.default
    //this class is implemented as a Singleton
    static let sharedInstance = BTCrane()
    //storing scan results
    public var peripherals = [Peripheral]()
    //connected peripheral
    public var connectedPeripheral:Peripheral?
    //connection state
    var isConnected = false
    var scanPerformed = false
    //crane status
    var craneStat:CraneStat = CraneStat()
    var statusTimer = Timer()
    var stausInterval:Double = 0.1
    
    //closures
    var scanFinishedHandler: (() -> Void)?
    var statusHandler: ((CraneStat) -> ())?
    
    //common commands
    let pitchRollCommand = Data(hexString: "060123000064a706012200005397")
    
    var pitchCommand    = BTMessage(channel: 0x06, cmd: 0x01, index: 0x23, data: 0x0000)  //tilt??
    var rollCommand     = BTMessage(channel: 0x06, cmd: 0x01, index: 0x22, data: 0x0000)
    var panCommand      = BTMessage(channel: 0x06, cmd: 0x01, index: 0x24, data: 0x0000)
    var modusCommand    = BTMessage(channel: 0x06, cmd: 0x01, index: 0x27, data: 0x0000)
    var batteryCommand  = BTMessage(channel: 0x06, cmd: 0x01, index: 0x06, data: 0x0000)
    //scan devices
    public func scan4Devices() {
        
        log.debug("scanning for devices")
        var tmpPeripherals = [UUID:Peripheral]()
        peripherals = [Peripheral]()
        
        SwiftyBluetooth.scanForPeripherals(withServiceUUIDs: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey : false], timeoutAfter: 2) { scanResult in
            switch scanResult {
            case .scanStarted:
                // The scan started meaning CBCentralManager scanForPeripherals(...) was called
                self.log.debug("start scan")
            case .scanResult(let peripheral, let advertisementData, let rssi):
                
                tmpPeripherals[peripheral.identifier] = peripheral

                self.log.debug("device = \(String(describing: peripheral.name))")
                self.log.debug("uuid = \(String(describing: peripheral.identifier))")
                self.log.debug("rssi = \(String(describing: rssi?.magnitude))")
                self.log.debug("adv = \(advertisementData)")

            case .scanStopped(let code):
                // The scan stopped, an error is passed if the scan stopped unexpectedly
                self.log.debug("stopped with code \(String(describing: code))")
                self.log.debug(" count = \(tmpPeripherals.count)")
                self.peripherals = Array(tmpPeripherals.values)
                self.scanPerformed = true
                self.log.debug(" count p = \(self.peripherals.count)")
                self.scanFinishedHandler?()
            }
        }
    }
    
    //connect to a peripheral - must be executed after a scan!!!
    public func connect(selected:Int) {
        peripherals[selected].connect(withTimeout: 2) { result in
            switch result {
            case .success:
                self.log.debug("connected")
                self.connectedPeripheral = self.peripherals[selected]
                self.isConnected = true
                self.addObserver(selected: selected)
                self.startUpdateStatus()
                
                /* can be removed as services are well known
                self.peripherals[selected].discoverServices(withUUIDs: nil) { result in
                    switch result {
                    case .success(let services):
                        


                        
                        for service in services {
                            
                            self.log.debug("service = \(service.description)")
                            self.peripherals[selected].discoverCharacteristics(withUUIDs: nil, ofServiceWithUUID: service.uuid) { result in
                                // The characteristics discovered or an error if something went wrong.
                                switch result {
                                case .success(let characteristics):
                                    for charac in characteristics {
                                        //self.log.debug("characteristic = \(charac.description)")
                                    }
                                break // An array containing all the characs requested.
                                case .failure(let error):
                                    self.log.debug("error = \(error)")
                                    break // A connection error or an array containing the UUIDs of the charac/services that we're not found.
                                }
                            }
                        }
                 
                        
                        break // An array containing all the services requested
                    case .failure(let error):
                        self.log.debug("error = \(error)")
                        break // A connection error or an array containing the UUIDs of the services that we're not found
                    }
 
                }
                */
                
                
                break // You are now connected to the peripheral
                case .failure(let error):
                    self.log.debug("error = \(error)")
                    break // An error happened while connecting
            }
        }
    }
    
    func addObserver(selected:Int) {
        log.debug("adding observer")
        NotificationCenter.default.addObserver(forName: Peripheral.PeripheralCharacteristicValueUpdate, object: self.peripherals[selected], queue: nil) { (notification) in
            //here we are receiving the notifications once they occur. Casting to characteristic
            let charac = notification.userInfo!["characteristic"] as! CBCharacteristic
            //process the data
            self.processCraneResponseData(response: charac.value)
            
            
            //let x  = self.peripherals[selected].characteristic(withUUID: "D44BC439-ABFD-45A2-B575-925416129601", ofServiceWithUUID: "0000FEE9-0000-1000-8000-00805F9B34FB")?.value
            //self.log.debug("Notyfying data in Observer \(String(describing: x?.hexEncodedString(options: .upperCase)))")
            if let error = notification.userInfo?["error"] as? SBError {
                self.log.debug("notification error = \(error)")
                // Deal with error
            }
        }
        
        self.peripherals[selected].setNotifyValue(toEnabled: true, forCharacWithUUID: "D44BC439-ABFD-45A2-B575-925416129601", ofServiceWithUUID: "0000FEE9-0000-1000-8000-00805F9B34FB") { (error) in
            // If there were no errors, you will now receive Notifications when that characteristic value gets updated.
            //if something goes wrong I should try to handle the error...
            self.log.debug("Notyfying  debug description \(error.debugDescription)")
        }
    }
    
    
    
    //regular update of crane status
    func startUpdateStatus() {
        statusTimer = Timer.scheduledTimer(timeInterval: stausInterval , target: self,   selector: (#selector (updateStatus)), userInfo: nil, repeats: true)
    }
    
    func stopUpdateStatus() {
        statusTimer.invalidate()
    }
    
    @objc func updateStatus() {
        //log.debug("update Status")
        //writeCoreData(data: pitchRollCommand!)
        execCommands(cmd1: &rollCommand, cmd2: &pitchCommand)
        execCommands(cmd1: &panCommand, cmd2: &modusCommand)
        execCommands(cmd1: &batteryCommand)
        statusHandler?(craneStat)
    }
    
    //core data processing
    //calculate the CRC (ccitt-xmodem)
    func crc16ccitt(data: [UInt8],seed: UInt32 = 0x0000, final: UInt32 = 0xffff)->UInt16{
        var crc = seed
        data.forEach { (byte) in
            crc ^= UInt32(byte) << 8
            (0..<8).forEach({ _ in
                crc = (crc & UInt32(0x8000)) != 0 ? (crc << 1) ^ 0x1021 : crc << 1
            })
        }
        return UInt16(crc & final)
    }
    
    //convert signed int into float and divide by 100 to get degree of axis --> todo make extension
    func int2float(value: UInt16) -> Float {
        let tmp : Int
        if value > UInt16.max / 2 {
            tmp = -Int(~value + 1) // ~ is the Bitwise NOT Operator
        } else {
            tmp = Int(value)
        }
        
        return Float(tmp)/100.0
    }
    
    
    //each response should at least 7 bytes long
    // |BLE Channel|CMD+Addr|Index|Data H|Data L|CRC H| CRC L|
    func processCraneResponseData(response:Data?) {
        
        
        var i:Int
        var tmp:BTMessage = BTMessage()
        
        //do we have a proper response?
        if let response = response {
            //log.debug("resonse length = \(response.count)")
            if response.count < 7  { return } //incomplete answer, nothing to do here
            //log.debug("Processing response \(String(describing: response.hexEncodedString(options: .upperCase)))")
            i = 0
            
            for byte:UInt8 in response {
                
                
                //log.debug (" i = \(i)")
                if i == 0 {
                    tmp = BTMessage()
                    tmp.channel = byte
                } else if i == 1 {
                    tmp.cmd = byte
                } else if i == 2 {
                    tmp.index = byte
                } else if i == 3 {
                    tmp.data = 256 * UInt16(byte)
                } else if i == 4 {
                    tmp.data = tmp.data + UInt16(byte)
                } else if i == 5 {
                    tmp.crc = 256 * UInt16(byte)
                } else if i == 6 {
                    tmp.crc = tmp.crc + UInt16(byte)
                    processSingleCraneResponse(response: tmp)
                    i = -1
                    //log.debug ("von vorne")
                }
                
                i = i + 1
                
            }
        }
    }
    
    func processSingleCraneResponse(response:BTMessage) {
        //log.debug("single response = \(response)")
        //log.debug("float value = \(response.data2float())")
        
        //take action on given index
        let index = response.index
        
        switch index {
            case 0x22: //pitch
                craneStat.tilt = response.data2float()
                break
            case 0x23: //roll
                craneStat.roll = response.data2float()
            case 0x24: //yaw
                craneStat.pan = response.data2float()
            case 0x27: //modus
                craneStat.modus = response.data
            case 0x06: //battery
                //craneStat.battery = Int(response.data)
            //log.debug("battery = \(response.data)  \(response.data2float())")
            break
            default:
                break  //should not happen...
                //log.debug("single response = \(response.toString())")
        }
    }
    
    
    
    func writeCoreData(data:Data) {
        connectedPeripheral?.writeValue(ofCharacWithUUID: "D44BC439-ABFD-45A2-B575-925416129600",
                                        fromServiceWithUUID: "0000FEE9-0000-1000-8000-00805F9B34FB",
                                        value: data,
                                        type: CBCharacteristicWriteType.withoutResponse) { result in
                                            switch result {
                                            case .success:
                                                //self.log.debug("success FEE8-1 = \(result.value)")
                                                break // The write was succesful.
                                            case .failure(let error):
                                                self.log.debug("error = \(error)")
                                                break // An error happened while writting the data.
                                            }
        }
    }
    
    func execCommands(cmd1:inout BTMessage, cmd2:inout BTMessage) {
        var rawData = Data()
        
        rawData.append(cmd1.toData())
        rawData.append(cmd2.toData())
        
        writeCoreData(data: rawData)
        //self.log.debug("exec data \(rawData.hexEncodedString(options: .upperCase))")
    }
    
    func execCommands(cmd1:inout BTMessage) {
        var rawData = Data()
        
        rawData.append(cmd1.toData())
        
        
        writeCoreData(data: rawData)
        //self.log.debug("exec data \(rawData.hexEncodedString(options: .upperCase))")
    }
    
    //high level commands
    
    func setCraneModus(modus:UInt16) {
        let m = modus % 3  // just to be sure that only allowed values are send to Crane
        var cmd = BTMessage(channel: 0x06, cmd: 0x81, index: 0x27, data: m)
        execCommands(cmd1: &cmd)
        
        
    }
    
    func performTilt(speed:UInt16) {
        var cmd = BTMessage(channel: 0x06, cmd: 0x10, index: 0x01, data: speed)
        execCommands(cmd1: &cmd)
    }
    
    
    
}
