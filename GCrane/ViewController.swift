//
//  ViewController.swift
//  GCrane
//
//  Created by Holger Kremmin on 27.02.18.
//  Copyright © 2018 Holger Kremmin. All rights reserved.
//

import UIKit
import XCGLogger
import SwiftyBluetooth
import CoreBluetooth
import SpriteKit


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverPresentationControllerDelegate{
    
    
    

    //members
    let log = XCGLogger.default
    var info = false
    var statusTimer: Timer?
    

    
    @IBOutlet weak var iosBattery: BatteryIndicator!
    @IBOutlet weak var craneBattery: BatteryIndicator!
    @IBOutlet weak var joystickView: SKView!
    @IBOutlet weak var panLabel: UILabel!
    @IBOutlet weak var tiltLabel: UILabel!
    @IBOutlet weak var rollLabel: UILabel!
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var panStackView: UIStackView!
    @IBOutlet weak var flightView: PrimaryFlightDisplayView!
    
    @IBOutlet weak var btButton: UIButton!
    @IBOutlet weak var degreePicker: UIPickerView!
    @IBOutlet weak var panRangePicker: UIPickerView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var modusButton: UIButton!
    
    
    
    //background for stackview-labels
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.layer.cornerRadius = 10.0
        return view
    }()
    
    private lazy var topStackBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.layer.cornerRadius = 10.0
        return view
    }()
    
    //app state
    private var flightMode = false

    
    //storing scan results
    
    private var peripherals = [Peripheral]()
    private var connectedPeripheral:Peripheral?
 
    //device status
    let motionKit = MotionKit()
    var btCrane = BTCrane.sharedInstance
    var iosBatteryLevel: Double {
        return Double(UIDevice.current.batteryLevel * 100)
    }
    
    //var peripheralIdentifier: PeripheralIdentifier?
    //private var btDevice:Peripheral?
    
    var leftHandler: ((AnalogJoystickData) -> ())?
    var rightHandler: ((AnalogJoystickData) -> ())?
    
    //speed constant for joystick
    
    let zeroSpeed = 2048.0
    
    //ui
    private func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    //overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //picker
        degreePicker.delegate = self
        degreePicker.dataSource = self
        panRangePicker.delegate = self
        panRangePicker.dataSource = self
        //labels
        panLabel.text = "-179.9"
        tiltLabel.text = "-179.9"
        rollLabel.text = "-179.9"
        
        pinBackground(backgroundView, to: infoStackView)
        
        iosBattery.percentCharged = 80
        craneBattery.percentCharged = 30
        
        //make record button round
        recordButton.layer.shadowColor = UIColor.black.cgColor
        recordButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        recordButton.layer.masksToBounds = false
        recordButton.layer.shadowRadius = 1.0
        recordButton.layer.shadowOpacity = 0.5
        recordButton.layer.cornerRadius = recordButton.frame.width / 2
        
        //battery
        //get notified by IOS battery changes
        NotificationCenter.default.addObserver(self, selector: #selector(batteryLevelDidChange), name: .UIDeviceBatteryLevelDidChange, object: nil)
        //startScanning()
        
        setStatusTimer(running: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let scene = JoystickScene(size: CGSize(width: self.joystickView.frame.size.width, height: self.joystickView.frame.size.height))
        scene.backgroundColor = .clear
        
        leftHandler = { [unowned self] data in
            
            let speedX = UInt16(self.zeroSpeed + (Double(data.velocity.x) * self.zeroSpeed))
            let speedY = UInt16(self.zeroSpeed + (Double(data.velocity.y) * self.zeroSpeed))
            
            
            self.log.debug("left speedx = \(speedX)  + speedy = \(speedY)")
            
            self.log.debug(" left x.velo =  \(data.velocity.x)  y.velo = \(data.velocity.y)  angle = \(data.angular)")
            //self.log.debug("angular = \(Double(data.angular) * (180.0 / Double.pi))  x Anteil \(cos(data.angular))  y Anteil  \(sin(data.angular))")
        }
        
        rightHandler = { [unowned self] data in
            
            let speedX = UInt16(self.zeroSpeed + (Double(data.velocity.x) * self.zeroSpeed))
            let speedY = UInt16(self.zeroSpeed + (Double(data.velocity.y) * self.zeroSpeed))
            
            if ( self.btCrane.craneStat.modus < 2) {
                self.btCrane.performTilt(speed: speedY)
            }
            self.log.debug("right speedx = \(speedX)  + speedy = \(speedY)")
            //self.log.debug("right x.velo =  \(data.velocity.x)  y.velo = \(data.velocity.y) angle = \(data.angular)")
            //self.log.debug("angular = \(Double(data.angular) * (180.0 / Double.pi))  x Anteil \(cos(data.angular))  y Anteil  \(sin(data.angular))")
        }
        
        scene.addJoystickHandlers(leftHandler: leftHandler!, rightHandler: rightHandler!)
        
        joystickView.showsFPS = false
        joystickView.showsNodeCount = false
        joystickView.ignoresSiblingOrder = true
        joystickView.allowsTransparency = true
        joystickView.backgroundColor = .clear
        joystickView.presentScene(scene)
        
        //view.addSubview(joystickView)
        flightView.isHidden = true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchMode() {
        if flightMode {
            
            //switch back to joystick mode
            //1. disable horizon views
            flightView.isHidden = true
            
            //2. enable joystick & panview
            joystickView.isHidden = false
            panStackView.isHidden = false
            infoStackView.isHidden = false
            
            //3. switch mode flag
            stopMotionKit()
            flightMode = !flightMode
            
            //disable horizon views
        } else {
            //switch to flight mode
            
            //1. disable joystick views
            joystickView.isHidden = true
            panStackView.isHidden = true
            infoStackView.isHidden = true
            
            //2. enable horizon
            flightView.isHidden = false
            //3. switch mode flag
            startMotionKit()
            flightMode = !flightMode
            
        }
    }
    
    func setStatusTimer(running:Bool) {
        if running {
            statusTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateCraneStatus), userInfo: nil, repeats: true)
        } else {
            if let btTimer = statusTimer {
                btTimer.invalidate()
            }
        }
    
    }
    
    func updateModusButton(modus:UInt16) {
        switch modus {
        case 3:
            log.debug("Modus \(modus)")
        case 0: //Pan Follow
            modusButton.setTitle("Pan",for: .normal)
            modusButton.backgroundColor = .green
            //log.debug("Modus \(modus)")
        case 1: //Locking Mode
            modusButton.setTitle("Lock",for: .normal)
            modusButton.backgroundColor = .yellow
            //log.debug("Modus \(modus)")
        case 2: //Following Mode
            modusButton.setTitle("Follow",for: .normal)
            modusButton.backgroundColor = .red
            //log.debug("Modus \(modus)")
        default:
            log.debug("Default Modus \(modus)")
        }
    }
    
    @objc func updateCraneStatus() {
        panLabel.text = "\(btCrane.craneStat.pan)"
        tiltLabel.text = "\(btCrane.craneStat.tilt)"
        rollLabel.text = "\(btCrane.craneStat.roll)"
        updateModusButton(modus: btCrane.craneStat.modus)
        if btCrane.isConnected {
            btButton.backgroundColor = .blue
        } else {
            btButton.backgroundColor = .yellow
        }
    }
    
    
    @IBAction func btButtonPressed(_ sender: Any) {
        showBTPopover(base: topStackView, center: true)
    }
    
    
    @IBAction func flightButtonPressed(_ sender: Any) {
        switchMode()
    }
    
    @IBAction func modusButtonPressed(_ sender: Any) {
        let modus = btCrane.craneStat.modus + 1 // the set setCraneModus func will check that values are valid
        btCrane.setCraneModus(modus: modus)
    }
    

   
    
    func listDevices() {
        
        for device in peripherals {
            log.debug("device = \(String(describing: device.name))")
            //log.debug("rssi = \(device.rssi)")
            log.debug("ident = \(device.identifier)")
            
        }
 
    }
    
    
    


    
    // ---------   picker protocol
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        
        
        return 1 //just one line
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //log.debug("per count = \(pickerView.tag)")
        //return peripherals.count
        switch pickerView.tag {
        case 1:
            return Int(360/5)
        case 2:
            return Int(10/0.1)
        default:
            return 0
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        /*
        let device = peripherals[row]
        guard let title = device.name else {
            return "unknown"
        }
        
        return title
         */
       
        
        switch pickerView.tag {
        case 1:
            
            return "\(Int(row * 5))"
        case 2:
            
            return String(format: "%.1f", (Double(row) * 0.1))
        default:
            return ""
        }
        
       
 
    }
    
    //ios battery status
    @objc func batteryLevelDidChange(_ notification: Notification) {
        iosBattery.percentCharged = iosBatteryLevel
    }
    
    //Motion Kit
    //helper functions
    // get magnitude of vector via Pythagorean theorem
    func magnitude(roll: Double, yaw: Double, pitch: Double) -> Double {
        return sqrt(pow(roll, 2) + pow(yaw, 2) + pow(pitch, 2))
    }
    
    func radiansToDegress(radians: Double) -> Double {
        return radians * 180 / Double.pi
    }
    func startMotionKit() {
        //check orientation of device for correct calculation of values
        switch UIApplication.shared.statusBarOrientation {
        case .portrait:
            //do something
            self.log.debug("portrait")
        case .portraitUpsideDown:
            //do something
            self.log.debug("portrait down")
        case .landscapeLeft:
            self.log.debug("left")
            
        case .landscapeRight:
            self.log.debug("right")
            
        case .unknown:
            self.log.debug("unknown")
            
        }
        motionKit.getAttitudeFromDeviceMotion(0.1) {
            (attitude) -> () in
            var roll = attitude.roll  //tilt
            let pitch = attitude.pitch  //roll...
            let yaw = attitude.yaw  //pan
            var rotationMatrix = attitude.rotationMatrix
            var quaternion = attitude.quaternion
            
            var m = self.magnitude(roll: roll, yaw: yaw, pitch: pitch)
            
            //check for orientation on Main thread only
            OperationQueue.main.addOperation(){
                
                if UIApplication.shared.statusBarOrientation == .landscapeLeft {
                    roll = roll + Double.pi/2  //depends on landscape orientation
                }
            }
            
            
            self.flightView.setAttitude(rollRadians: pitch , pitchRadians: roll)
            self.flightView.setHeadingDegree(degree: 127)
            
            self.log.debug("yaw \(self.radiansToDegress(radians:yaw).rounded(toPlaces: 2))")
            self.log.debug("roll \(self.radiansToDegress(radians:roll).rounded(toPlaces: 2))")
            self.log.debug("pitch \(self.radiansToDegress(radians:pitch).rounded(toPlaces: 2))")
            
        }
    }
    
    
    
    func stopMotionKit() {
        //Make sure to call the required function when you're done
        motionKit.stopAccelerometerUpdates()
        motionKit.stopGyroUpdates()
        motionKit.stopDeviceMotionUpdates()
        motionKit.stopmagnetometerUpdates()
    }
    
    
    //popover
    //popover delegates
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        log.debug("yuppie")
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        log.debug("dismiss")
        
       
        
    }
    
    //bt popover
    func showBTPopover(base: UIView, center: Bool)
    {
        
        if let viewController = UIStoryboard(name: "BT_Popover", bundle: Bundle.main).instantiateViewController(withIdentifier: "bt_popover") as? BTPopoverViewController {
            
            let navController = UINavigationController(rootViewController: viewController)
            navController.modalPresentationStyle = .popover
            
            if let pctrl = navController.popoverPresentationController  {
                pctrl.delegate = self
                
                pctrl.sourceView = base
                if center {
                    pctrl.sourceRect = CGRect(x: 000,y: 100,width: 0,height: 0)
                } else {
                    pctrl.sourceRect = base.bounds //CGRect(x: 100,y: 100,width: 0,height: 0) // base.bounds
                }
                
                
                self.present(navController, animated: true, completion: nil)
            }
        }
    }
    
    
} //class




