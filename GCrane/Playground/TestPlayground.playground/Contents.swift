//: Playground - noun: a place where people can play

import UIKit
import Foundation

print ("hello")


struct AA {
    var x:Int = 0 {
        didSet {
            x = x % 3
        }
    }
}

var z = AA()

z.x = 3

print (z.x)



/*
func crc16ccitt(data: [UInt8],seed: UInt32 = 0x0000, final: UInt32 = 0xffff)->UInt16{
    var crc = seed
    data.forEach { (byte) in
        crc ^= UInt32(byte) << 8
        (0..<8).forEach({ _ in
            crc = (crc & UInt32(0x8000)) != 0 ? (crc << 1) ^ 0x1021 : crc << 1
        })
    }
    return UInt16(crc & final)
}

func int2float(value: UInt16) -> Float {
    let tmp : Int
    if value > UInt16.max / 2 {
        tmp = -Int(~value + 1) // ~ is the Bitwise NOT Operator
    } else {
        tmp = Int(value)
    }
    
    return Float(tmp)/100.0
}

extension Data {
    func copyBytes<T>(as _: T.Type) -> [T] {
        return withUnsafeBytes { (bytes: UnsafePointer<T>) in
            Array(UnsafeBufferPointer(start: bytes, count: count / MemoryLayout<T>.stride))
        }
    }
}

//print(crc16ccitt(data: "".utf8.map{$0}) == 0x1d0f)
//print(crc16ccitt(data: "A".utf8.map{$0}) == 0x9479)
//print(crc16ccitt(data: "123456789".utf8.map{$0}) == 0xe5cc)
let testdata:[UInt8] = [0x06, 0x01, 0x24, 0xff, 0xa5]
print(crc16ccitt(data: testdata) == 0x0787)



let value:UInt16 = 0xFFF5
//var hex = UInt32(0xfffff830)



print(int2float(value: value))


let size = MemoryLayout<Int16>.stride
let data = Data(bytes: [0x06, 0x01, 0x24, 0xff, 0xa5, 0x07, 0x87,6,1, 0, 2, 0, 3, 0, 1, 2, 3, 4]) // little endian for 16-bit values
let int16s = data.withUnsafeBytes { (bytes: UnsafePointer<Int16>) in
    Array(UnsafeBufferPointer(start: bytes, count: data.count / size))
}

let length = data.count * MemoryLayout<Int16>.stride
var bytes1 = [Int16](repeating: 0, count: data.count / size)
(data as NSData).getBytes(&bytes1, length: length)

let bytes2 = data.withUnsafeBytes {
    UnsafeBufferPointer<Int16>(start: $0, count: data.count / size).map(Int16.init(littleEndian:))
}

let bytes3 = data.withUnsafeBytes {
    Array(UnsafeBufferPointer<Int16>(start: $0, count: data.count / size))
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}

struct BTMessage {
    var channel:UInt8 = 0
    var cmd:UInt8 = 0
    var data:UInt16 = 0
    var index:UInt8 = 0
    var crc:UInt16 = 0
    
    //convert signed int into float and divide by 100 to get degree of axis
    func data2float() -> Float {
        let tmp : Int
        if data > UInt16.max / 2 {
            tmp = -Int(~data + 1) // ~ is the Bitwise NOT Operator
        } else {
            tmp = Int(data)
        }
        
        return Float(tmp)/100.0
    }
    
    func crc16ccitt(data: [UInt8],seed: UInt32 = 0x0000, final: UInt32 = 0xffff)->UInt16{
        var crc = seed
        data.forEach { (byte) in
            crc ^= UInt32(byte) << 8
            (0..<8).forEach({ _ in
                crc = (crc & UInt32(0x8000)) != 0 ? (crc << 1) ^ 0x1021 : crc << 1
            })
        }
        return UInt16(crc & final)
    }
    
    func calcCRC() {
        let tmp:[UInt8] = [channel, cmd, index, UInt8(data >> 8), UInt8(data & 0x00ff)]
        
        for a in tmp {
            print ("byte = \(String(format:"%02X",a))")
        }
        let hexString = String(format:"%02X", crc16ccitt(data: tmp))
        print ("calculated crc = \(hexString)")
    }
}

func processCraneResponseData(response:Data?) {

    
    var i:Int
    var tmp:BTMessage = BTMessage()
    
    //do we have a proper response?
    if let response = response {
        print("resonse length = \(response.count)")
        if response.count < 7  { return } //incomplete answer, nothing to do here
        print("Processing response \(String(describing: response.hexEncodedString(options: .upperCase)))")
        i = 0
        
        for byte:UInt8 in response {
            
            
            print (" i = \(i)")
            if i == 0 {
                tmp = BTMessage()
                tmp.channel = byte
            } else if i == 1 {
                tmp.cmd = byte
            } else if i == 2 {
                tmp.index = byte
            } else if i == 3 {
                tmp.data = 256 * UInt16(byte)
            } else if i == 4 {
                tmp.data = tmp.data + UInt16(byte)
            } else if i == 5 {
                tmp.crc = 256 * UInt16(byte)
            } else if i == 6 {
                tmp.crc = tmp.crc + UInt16(byte)
                processSingleCraneResponse(response: tmp)
                i = 0
                print ("von vorne")
            }
            
            i = i + 1
            
        }
    }
}

func processSingleCraneResponse(response:BTMessage) {
    print("single response = \(response)")
    print("float value = \(response.data2float())")
    let hexString = String(format:"%02X", response.crc)
    print ("crc  = \(hexString)")
    response.calcCRC()
}

processCraneResponseData(response: data)
 
 */
